module Aspirator

  module Helpers

    # http://stackoverflow.com/a/5471032
    # Cross-platform way of finding an executable in the $PATH.
    #   which('ruby') #=> /usr/bin/ruby
    def which(cmd)
      exts = ENV['PATHEXT'] ? ENV['PATHEXT'].split(';') : ['']
      ENV['PATH'].split(File::PATH_SEPARATOR).each do |path|
        exts.each { |ext|
          exe = File.join(path, "#{cmd}#{ext}")
          return exe if File.executable? exe
        }
      end
      return nil
    end


    RESERVED_WORDS = %w(not)
    RE_CONSTANT = /([a-z]\w*|\d+)/
    RE_TERM_RAW = /([a-zA-Z][\w(),]*|\d+|_|"[^"]*")/
    RE_PREDICATE = /[a-z]\w*/
    RE_VARIABLE = /[A-Z]\w*/

    def legal_nonreserved?(word, re=nil)
      return false if RESERVED_WORDS.include?(word)
      re ? !!(re =~ word) : true
    end

    def legal_constant?(const)
      #legal_nonreserved? const, /\A([a-z][\w\d_]*|\d+)\Z/
      legal_nonreserved? const, /\A#{RE_CONSTANT}\Z/
    end

    # NOTE: This is a very rough check, that doesn't account for the structure of the term
    def legal_term?(const)
      legal_nonreserved? const, /\A#{RE_TERM_RAW}\Z/
    end

    def legal_predicate?(pred)
      #legal_nonreserved? pred, /\A[a-z][\w\d_]*\Z/
      legal_nonreserved? pred, /\A#{RE_PREDICATE}\Z/
    end

    def legal_variable?(var)
      #legal_nonreserved? var, /\A[A-Z][\w\d_]*\Z/
      legal_nonreserved? var, /\A#{RE_VARIABLE}\Z/
    end


    #module Observable
    #  def setup(klass, opts={})
    #    opts = opts.reverse_merge events: []
    #    raise ArgumentError, 'At least one event must be given' if opts[:events].empty?
    #
    #    add_class_methods(klass, opts)
    #    add_instance_methods(klass, opts)
    #  end
    #
    #  def add_class_methods(klass, opts)
    #    klass.OBSERVABLE_EVENTS = opts[:events]
    #
    #    #def klass.some_class_method do
    #    #
    #    #end
    #  end
    #
    #  def add_instance_methods(klass, opts)
    #    klass.class_eval do
    #      def initialize
    #        @observers = {}
    #        p "Observable Initialize!"
    #      end
    #
    #      def add_observer(event, &observer)
    #        raise ArgumentError, "Unknown event type '#{event}'" unless klass.OBSERVABLE_EVENTS.include? event
    #
    #        os = @observers.fetch(event, [])
    #        os << observer unless os.include? observer
    #        @observers[event] = os
    #      end
    #
    #      def remove_observer(observer)
    #        @observers.each_value do |os|
    #          os.delete(observer)
    #        end
    #      end
    #
    #      def notify_observers(event, *args)
    #        event_observers = @observers.fetch(event, [])
    #        return if event_observers.empty?
    #
    #        event_observers.each do |observer|
    #          observer.call(*args)
    #        end
    #      end
    #    end
    #  end
    #
    #  extend self
    #end

  end

end


Hash.class_eval do
  def reverse_merge(defaults = {})
    defaults.merge(self)
  end

  def default!(defaults = {})
    replace(defaults.merge(self))
  end
end


String.class_eval do
  def strip_pat(pat)
    chars = '[' << Regexp.escape(chars) << ']' unless chars.is_a? Regexp
    self.gsub(/\A#{chars}+|#{chars}+\Z/, "")
  end
end