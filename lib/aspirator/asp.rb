require "set"
require "aspirator/helpers"

module Aspirator

  # TODO:
  #
  # Asp/Extensions:
  #  Add pretty printing for answer sets (e.g. grouping by predicate)
  #  Add easy extraction of all literals of the same predicate
  #

  module Asp

    class ParsingError < Aspirator::AspiratorError;
    end

    class IllegalIdentifierError < ParsingError;
    end


    class Atom

      include Helpers

      attr_reader :predicate
      attr_reader :arguments

      alias :pred :predicate
      alias :args :arguments

      def initialize(pred, *args)
        pred = pred.to_s

        if args.length > 0
          args = args[0] if args[0].is_a?(Enumerable)
        end

        unless legal_predicate? pred
          raise IllegalIdentifierError, "#{pred} is not a legal predicate"
        end

        if (arg = args.detect { |a| not legal_term?(a) and not legal_variable?(a) })
          raise IllegalIdentifierError, "#{arg} is not a legal argument"
        end

        @predicate = pred
        @arguments = args.to_a
      end

      def eql?(other)
        if other.equal?(self)
          return true
        elsif !self.class.equal?(other.class)
          return false
        end

        other.predicate == self.predicate && other.arguments == self.arguments
      end
      alias_method :==, :eql?

      def hash
        h = 17
        h = h * 31 + @predicate.hash
        h = h * 31 + @arguments.hash
      end

      def to_s
        "#{@predicate}" << (@arguments.empty? ? "" : "(#{@arguments.join(', ')})")
      end

      def inspect
        "<#{self.class.to_s}: #{self.to_s}>"
      end
    end

    #class AtomSet < Set
    #  def initialize(atoms)
    #    raise ArgumentError, 'Elements of AtomSet must respond to :predicate and :arguments' \
    #      unless atoms.all? { |atom| atom.respond_to?(:predicate) && atom.respond_to?(:arguments) }
    #
    #    super(atoms)
    #  end
    #
    #  # TODO: Also check elements added in posterior for the correct interface
    #end


    class Literal
      attr_reader :negated, :atom
      alias :neg :negated

      def initialize(negated, atom)
        @negated = !!negated
        @atom = atom
      end

      def predicate
        @atom.predicate
      end

      def arguments
        @atom.arguments
      end

      alias :pred :predicate
      alias :args :arguments

      def self.parse(lit_str)
        m = /\A-/ =~ lit_str

        atom = Atom.parse(lit_str)

        if m
          Literal.new(true, atom)
        else
          Literal.new(false, atom)
        end
      end

      def eql?(other)
        if other.equal?(self)
          return true
        elsif !self.class.equal?(other.class)
          return false
        end

        other.negated == self.negated && self.atom.eql?(other.atom)
      end
      alias_method :==, :eql?

      def hash
        h = 17
        h = h * 31 + @negated.hash
        h = h * 31 + @atom.hash
      end

      def to_s
        (@negated ? "-" : "") << @atom.to_s
      end

      def inspect
        "<#{self.class.to_s}: #{self.to_s}>"
      end
    end


    class LiteralSet < Set

      #RE_ATOM = /([a-z][\w\d]*\([a-z][\w\d]*\))/
      #RE_MODEL = /^\{(#{RE_ATOM}(?:,\s*#{RE_ATOM})*)?\}$/

      def initialize(literals=[])
        raise ArgumentError, 'Elements of LiteralSet must respond to :negated, :predicate and :arguments' \
          unless literals.all? { |lit|
            lit.respond_to?(:negated) && lit.respond_to?(:predicate) && lit.respond_to?(:arguments)
          }
        super(literals)
      end

      # TODO: Also check elements added in posterior for the correct interface

      def to_s
        "{#{self.to_a.join(', ')}}"
      end

      def inspect
        "<#{self.class.to_s}: #{self.to_s}>"
      end

    end

    class AnswerSet < LiteralSet

    end

  end

end