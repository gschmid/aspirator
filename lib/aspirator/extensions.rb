require "set"
require "aspirator/asp"

# asp extensions

module Aspirator::Asp

  Atom.class_eval do
    def to_fact
      self.to_s << "."
    end
  end

  Literal.class_eval do
    def to_fact
      self.to_s << "."
    end
  end

  LiteralSet.class_eval do
    def to_program
      self.map(&:to_fact).join(' ')
    end

    def predicate_groups
      self.group_by{|x| x.predicate}
    end
  end

end


# core extensions

Enumerable.class_eval do
  def to_literals(predicate, negated=false)
    predicate = predicate.to_s
    Aspirator::Asp::LiteralSet.new self.map{ |args|
      args ||= []
      args = [args] unless args.is_a?(Enumerable)
      atom = Aspirator::Asp::Atom.new(predicate, args.map(&:to_s))
      Aspirator::Asp::Literal.new(negated, atom)
    }
  end
end

Hash.class_eval do
  # Transforms hashes of the form
  #   { predicate: [[fact0_arg0, ..., fact0_argN], ..., [factM_arg0, ..., factM_argN]], ... }
  # into an instance of Aspirator::Asp::LiteralSet.
  # `predicate` is a symbol representing the predicate of the atoms.
  #             If the symbol starts with `_` the atoms will be negated.
  def to_asp
    literals = Aspirator::Asp::LiteralSet.new

    self.each_pair do |posneg_pred, values|
      posneg_pred = posneg_pred.to_s

      negated = posneg_pred[0] == '_'
      posneg_pred.slice!(0) if negated

      raise ArgumentError, 'facts must be given as an Enumerable' unless values.is_a?(Enumerable)
      literals.merge values.to_literals(posneg_pred, negated)
    end

    literals
  end
end

Set.class_eval do
  def asp_prettysets
    self.map{|as| as.predicate_groups.each_value.map{|ls| ls.sort_by{|l| l.to_s}.join(' ') }.join("\n") }.join("\n--\n")
  end
end