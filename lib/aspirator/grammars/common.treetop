require "aspirator/asp"

module Aspirator::Grammars
  grammar CommonAsp

    rule ID_CHAR
      [a-zA-Z0-9_]
    end

    rule WS
      [\s]
    end

    rule STRONG_NEGATION_KEYWORD
      "-"
    end

    rule WEAK_NEGATION_KEYWORD
      "not"
    end

    rule RULE_BODY_KEYWORD
      ":-"
    end

    rule RULE_END_KEYWORD
      "."
    end


    rule constant
      [a-z] ID_CHAR*
      /
      ([1-9] [0-9]* / "0")
      /
      "\"" [^"]* "\""
    end

    rule function_id
      [a-z] ID_CHAR*
    end

    rule variable
      [A-Z] ID_CHAR*
    end

    rule variable_or_anonymous
      variable / "_"
    end

    rule predicate
      [a-z] ID_CHAR*
    end


    rule term
      constant
      / function_id tail:( "(" WS* arguments:term_list WS* ")" )?
      / variable_or_anonymous
    end

    rule term_list
      first:term tail:( WS* "," WS* term )*
      {
        def terms
          #puts "tail = ..."
          #tail.elements.each_with_index {|x,i| puts "%d: %s\n term = %s" % [i+1, x.inspect, x.term.inspect] }
          [first] + tail.elements.map(&:term)
        end
      }
    end


    rule atom
      pred:predicate tail:( "(" WS* term_list WS* ")" )?
      {
        def predicate
          pred.text_value
        end

        def arguments
          tail.empty? ? [] : tail.term_list.terms
        end

        def to_asp
          Aspirator::Asp::Atom.new(predicate, arguments.map(&:text_value))
        end
      }
    end

    rule literal
      neg:STRONG_NEGATION_KEYWORD? atom
      {
        def negated?
          ! neg.empty?
        end

        def to_asp
          Aspirator::Asp::Literal.new(negated?, atom.to_asp)
        end
      }
    end


    #rule rule_head
    #
    #end

    #rule rule_body
    #
    #end

    #rule rule_full
    #  rule_head ( WS* RULE_BODY_KEYWORD WS* rule_body ) WS* RULE_END_KEYWORD
    #end


    rule dlv_answer_set
      "{" WS* inner:( first:literal tail:( WS* "," WS* literal )* WS* )? "}"
      {
        def literals
          return [] if inner.empty?
          [inner.first] + inner.tail.elements.map{|x| x.literal }
        end

        def to_asp
          Aspirator::Asp::AnswerSet.new( literals.map(&:to_asp) )
        end
      }
    end


    rule clingo_answer_set
      first:literal tail:( WS* literal )*
      {
        def literals
          return [] if empty?
          [first] + tail.elements.map{|x| x.literal }
        end

        def to_asp
          Aspirator::Asp::AnswerSet.new( literals.map(&:to_asp) )
        end
      }
    end

  end
end