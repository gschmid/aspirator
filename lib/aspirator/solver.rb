require "open3"
require "timeout"

require "aspirator/helpers"
require "aspirator/asp"

require "treetop"
require "aspirator/grammars/common"


module Aspirator

  # TODO:
  #
  # Solver:
  #  Handle DLV warnings
  #  Handle passed solver arguments cleanly (i.e. ensure each option occurs once at most)
  #  Attach optimization value to found answer sets, so they can be invalidated later on (or at least logged)
  #  Handle multiple dimensions of optimization goal values properly
  #
  # Tests:
  #  Add tests for general warnings/errors and solver options
  #

  class Solver
    include Helpers

    private_class_method :new # abstract

    def initialize(solver_path, opts={})
      @solver_path = solver_path

      @opts = opts.reverse_merge  filter: [],                   # only report specific predicates
                                  filter_positive_only: false,  # only filter out positive literals
                                  raw: false,                   # do not interpret any of the output
                                  raise: true,                  # raise errors when solver returns non-answer-set line
                                  log_output: true,             # whether to record all of the solver's output
                                  log_warnings: true,           # whether to record the solver's warnings separately
                                  solver_args: [],              # arguments passed directly to the solver executable
                                  all_optimal: false,           # rerun the solver in order to compute all
                                                                #  optimal answer sets
                                  timeout_min: nil,             # when different from nil, stop the solver after the
                                                                #  given duration, if some answer set has been found
                                  timeout_max: nil              # when different from nil, stop the solver after the
                                                                #  given duration
    end


    private

    def solver_args(opts)
      raise NotImplementedError
    end

    def on_begin_run(run)
      # No Op
    end

    def on_result_line(run, line)
      raise NotImplementedError
    end

    def on_end_run(run)
      # No Op
    end


    public

    def run(program, opts={})
      raise ProgramSyntaxError if program.nil?

      opts = opts.reverse_merge @opts
      run = Run.new(program, opts.dup)

      on_begin_run(run)

      Open3::popen2e("#{@solver_path} #{solver_args(opts)}") { |stdin, stdout, wait_thr|
        stdin.write program.to_s
        stdin.close

        t_start = Time.now
        timeout_min = opts[:timeout_min]
        timeout_max = opts[:timeout_max]

        begin
          line = nil

          loop do
            begin
              Timeout::timeout(0.050) do
                line = stdout.readline
              end
              run.output << line if opts[:log_output]
              on_result_line(run, line.rstrip)

            rescue Timeout::Error
              duration = Time.now - t_start
              break if timeout_min && duration > timeout_min && !run.answer_sets.empty?
              break if timeout_max && duration > timeout_max
            end
          end

        rescue EOFError
          # Done
        end

        stdout.close
      }

      on_end_run(run)

      run
    end


    class Run
      attr_reader :program, :opts, :output, :answer_sets, :errors, :warnings
      attr_accessor :optimal_value

      def initialize(program, opts={}, output=[], answer_sets=nil, errors=[], warnings=[], optimal_value=nil)
        @program = program
        @opts = opts
        @output = output
        @answer_sets = answer_sets || Set.new
        @errors = errors
        @warnings = warnings
        @optimal_value = optimal_value
      end

      def successful?
        @answer_sets && @errors.empty?
      end

      def warnings?
        ! @warnings.empty?
      end

      def answer_sets?
        ! @answer_sets.empty?
      end

      def replace_with_optimal(optimal_run)
        @output = optimal_run.output
        @answer_sets = optimal_run.answer_sets
      end
    end

  end


  class DlvSolver < Solver

    public_class_method :new # concrete

    DLV_COMMAND = 'dlv'


    def initialize(solver_opts={})
      #puts "DlvSolver init: #{solver_opts.inspect}"

      solver_path = which(DLV_COMMAND)
      raise MissingSolverError, "DLV executable could not be located" if solver_path.nil?

      super(solver_path, solver_opts)

      #puts "DlvSolver args: #{solver_args}"
    end


    private

    def solver_args(opts=nil)
      opts ||= @opts

      unless opts[:filter].empty?
        filter_opt = opts[:filter_positive_only] ? '-pfilter=' : '-filter='
        filter_opt << opts[:filter].join(',')
      end

      "#{filter_opt} -silent --"
    end

    def on_begin_run(run)
      #puts "Begin run"
    end

    def on_result_line(run, line)
      #puts "New line: #{line}"

      # Ignore empty lines
      return if /^\s*$/ =~ line

      # Ignore Cost lines
      # TODO: Convert / Save this as a meaningful value similar to run.optimal_value
      if /^Cost / =~ line
        return
      end

      # Strip "Best model: " (occurs in optimization problems)
      if (m = line.match /^(Best model: )/)
        line = m.post_match
      end

      # Try to parse as answer-set
      begin
        run.answer_sets << self.class.parse_answer_set(line)

      rescue Asp::ParsingError => e
        if run.opts[:raise]
          raise SolverError, "Solver returned unparseable line: #{line}; Reason: #{e}"
        else
          run.errors << line
        end
      end
    end

    def on_end_run(run)
      #puts "End run"
    end


    public

    def self.parse_answer_set(set_str)
      #m = set_str.match /^\s*\{([^{}]*)\}\s*$/
      #raise Asp::ParsingError, 'Parsing the given string as a literal set failed' unless m
      #literals = m[1].split(',').map(&:strip).map{|x| x.empty? ? nil : x }.compact
      #Asp::AnswerSet.new( literals.map{|lit_str| Asp::Literal.parse(lit_str) } )

      parser = Grammars::CommonAspParser.new
      res = parser.parse(set_str, root: :dlv_answer_set)
      raise Asp::ParsingError, "Parsing the given string as a literal set failed: #{parser.failure_reason}" unless res

      res.to_asp
    end

  end


  class ClingoSolver < Solver

    public_class_method :new # concrete

    #CLINGO_COMMAND = 'clingo'
    GRINGO_COMMAND = 'gringo'
    CLASP_COMMAND = 'clasp'


    def initialize(solver_opts={})
      gringo_path = which(GRINGO_COMMAND)
      raise MissingSolverError, "Gringo executable could not be located" if gringo_path.nil?
      clasp_path = which(CLASP_COMMAND)
      raise MissingSolverError, "Clasp executable could not be located" if clasp_path.nil?

      super("#{gringo_path} | #{clasp_path}", solver_opts)
    end


    private

    def solver_args(opts=nil)
      opts ||= @opts
      args = ["-n #{opts[:num_solutions] || 0}", "--verbose=0"]

      # FIXME: Implement filters for Clingo
      #unless opts[:filter].empty?
      #  filter_opt = opts[:filter_positive_only] ? '-pfilter=' : '-filter='
      #  filter_opt << opts[:filter].join(',')
      #end

      args += opts[:solver_args] || []

      args.join(' ')
    end

    #def on_begin_run(run)
    #end

    def on_result_line(run, line)
      #puts "New line: #{line}"

      # Ignore empty/irrelevant lines
      return if /^\s*$/ =~ line

      # Record warnings
      # NOTE: We consider ALL LINES BEGINNING WITH WHITESPACE as warnings!
      if /(^% warning:|^[^\s]+ warning:|^\s+)/ =~ line
        run.warnings << line if run.opts[:log_warnings]
        return
      end

      # Track optimization goal value
      if (m = line.match /^Optimization: (\d+)/)
        run.optimal_value = m[1].to_i
        return
      end

      # Special cases
      if "SATISFIABLE" == line
        run.answer_sets << Aspirator::Asp::AnswerSet.new if run.answer_sets.empty?
        return

      elsif "OPTIMUM FOUND" == line
        if run.answer_sets.empty?
          run.answer_sets << Aspirator::Asp::AnswerSet.new

        else
          # Rerun with optimum given
          if run.opts[:all_optimal]
            opts2 = run.opts.merge all_optimal: false
            opts2[:solver_args] += ["--opt-all=#{run.optimal_value}"]

            # Compute ALL optimal answer sets (and NONE of the sub-optimal):
            run.replace_with_optimal self.run(run.program, opts2)
          end
        end

        return

      elsif "UNSATISFIABLE" == line
        run.answer_sets.clear # redundant, since no answer sets should have been returned anyway
        return
      end

      # Try to parse as answer-set
      begin
        run.answer_sets << self.class.parse_answer_set(line)

      rescue Asp::ParsingError => e
        if run.opts[:raise]
          raise SolverError, "Solver returned unparseable line: #{line}"
        else
          run.errors << line
        end
      end
    end

    #def on_end_run(run)
    #end


    public

    def self.parse_answer_set(set_str)
      parser = Grammars::CommonAspParser.new
      res = parser.parse(set_str, root: :clingo_answer_set)
      raise Asp::ParsingError, "Parsing the given string as a literal set failed: #{parser.failure_reason}" unless res

      res.to_asp
    end

  end

end