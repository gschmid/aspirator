require "aspirator/version"

module Aspirator

  class AspiratorError < StandardError;
  end

  class MissingSolverError < AspiratorError;
  end

  class SolverError < AspiratorError;
  end

  class ProgramSyntaxError < SolverError;
  end

end

require "aspirator/asp"
require "aspirator/solver"