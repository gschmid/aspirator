require_relative 'spec_helper'
require 'aspirator'

def parse_dlv_answer_sets(*as_strs)
  Set.new( as_strs.map{ |as_str| Aspirator::DlvSolver.parse_answer_set(as_str) } )
end

[Aspirator::DlvSolver, Aspirator::ClingoSolver].each do |solver_class|

  _Solver = solver_class
  _OptSep = _Solver == Aspirator::DlvSolver ? ':' : '@'

  describe _Solver do

    #Solver = Aspirator::DlvSolver

    let(:valid_program) { "person(adam). person(peter). person(mary). iamfine." }
    let(:invalid_program) { "Invalid_Predicate!(adam)." }

    let(:solver) { subject }

    context 'basic validation' do
      it 'should successfully run a valid program' do
        run = solver.run valid_program
        run.errors.should be_empty
        run.warnings.should be_empty
      end

      it 'should throw ProgramSyntaxError when given a syntactically invalid program' do
        error = Aspirator::SolverError

        expect { solver.run invalid_program }.to raise_error(error)

        expect { solver.run "person(_invalidConstant)." }.to raise_error(error)

        expect { solver.run "person(0invalidConstant)." }.to raise_error(error)

        expect { solver.run "0person." }.to raise_error(error)
      end
    end

    context 'answer sets' do
      #let(:solver) { Solver.new raise: false }

      it 'should return all answer sets' do
        # some valid facts, single answer set
        run = solver.run valid_program
        run.answer_sets.should == parse_dlv_answer_sets( "{person(adam), person(peter), person(mary), iamfine}" )
      end

      it 'should return all answer sets' do
        # valid, a singular empty answer set
        run = solver.run "a :- b."
        run.answer_sets.should == parse_dlv_answer_sets( "{}" )
      end

      it 'should return all answer sets' do
        # valid, but two answer sets
        run = solver.run "b :- not a. a :- not b. c."
        run.answer_sets.should == parse_dlv_answer_sets( "{a, c}", "{b, c}" )
      end

      it 'should return all answer sets' do
        # valid, but no answer set
        run = solver.run "a. :- a."
        run.answer_sets.should be_empty
      end

      it 'should return answer sets with strings' do
        run = solver.run "name(\"peter\"). name(\"c3p0_!\")."
        run.answer_sets.should == parse_dlv_answer_sets( "{name(\"peter\"), name(\"c3p0_!\")}" )
      end
    end

    # TODO: Test solver options

    context 'options' do

      context 'all_optimal' do
        it 'should return all optimal answer sets and no others' do
          run = solver.run "a :- not b, not c. b :- not a, not c. c :- not a, not b. :~ a. [1#{_OptSep}1]", all_optimal: true
          run.answer_sets.should == parse_dlv_answer_sets( "{b}", "{c}" )
        end
      end

    end
  end

end