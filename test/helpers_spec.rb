require_relative 'spec_helper'
require 'aspirator/helpers'

describe Aspirator::Helpers do

  include Aspirator::Helpers

  let(:sample_positives) { %w(predicate predicate123 preDIC4te a000 ze_ro) }
  let(:sample_negatives) { %w(Large _aaa 1aaa down-up not) }

  context 'legal_predicate?' do
    it 'should validate well-formed predicates' do
      sample_positives.each{|pred|
        legal_predicate?(pred).should be_true
      }
    end

    it 'should invalidate other predicates' do
      (sample_negatives + %w(123)).each{|pred|
        legal_predicate?(pred).should be_false
      }
    end
  end

  context 'legal_constant?' do
    it 'should validate well-formed constants' do
      (sample_positives + %w(123)).each{|c|
        legal_constant?(c).should be_true
      }
    end

    it 'should invalidate other constants' do
      sample_negatives.each{|c|
        legal_constant?(c).should be_false
      }
    end
  end

  context 'legal_term?' do
    it 'should validate plausible terms' do
      %w(a b9 c_1 test peter_parker X Variable 123 _).each{|c|
        legal_term?(c).should be_true
      }
    end

    it 'should invalidate implausible terms' do
      # i.e. such that contain restricted characters
      %w(_aaa _VVV 123aaa test! -something not).each{|c|
        legal_term?(c).should be_false
      }
    end
  end

  context 'legal_variable?' do
    it 'should validate well-formed variables' do
      %w(V Abacus X123 X_a2C).each{|v|
        legal_variable?(v).should be_true
      }
    end

    it 'should invalidate other variables' do
      sample_positives.each{|v|
        legal_variable?(v).should be_false
      }
    end
  end
end