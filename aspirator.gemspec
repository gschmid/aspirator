# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'aspirator/version'

Gem::Specification.new do |spec|
  spec.name          = "aspirator"
  spec.version       = Aspirator::VERSION
  spec.authors       = ["Georg Schmid"]
  spec.email         = ["gspschmid@gmail.com"]
  spec.description   = "A gem that acts as a bridge from Ruby to ASP solvers such as DLV and Clingo."
  spec.summary       = "Gotta write a nice summary."
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  # TODO: Add these as production dependencies and switch to automatic grammar compilation
  #gem 'polyglot'
  #gem 'treetop'

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "treetop"
end
