![Aspirator](https://bitbucket.org/gschmid/aspirator/raw/74b258559644fcbd8bf4b269d24ad7cc1581d08f/doc/aspirator_330x107.png)

Aspirator is a Ruby wrapper for ASP solvers such as [DLV][1] and the [Potassco Suite][2].

Please note that this is in a very early stage. Feel free to build upon it.

## Installation

Add this line to your application's Gemfile:

    gem 'aspirator'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install aspirator

## Usage

Some exemplary usage:

```ruby
require "aspirator"


# Set up gringo & clasp (i.e. clingo), without having it  raise Errors and a
#   maximum timeout of 15 seconds

solver = Aspirator::ClingoSolver.new raise: false, timeout_max: 15


# The answer set program is given an ordinary string

program = <<-PROGRAM

% node(N).   ... some Node N
% edge(A,B). ... directed Edge from Node A to Node B

reachable_from(B, A) :- edge(A, B).
reachable_from(C, A) :- edge(A, B), edge(B, C).

#show reachable_from/2.

PROGRAM


# Setup "external" input via the ASP objects from Aspirator::Asp representing
#   Literals and sets thereof

include Aspirator::Asp

literals = LiteralSet.new [
    Literal.new(false, Atom.new(:node, "a")),       # -> node(a).
    Literal.new(false, Atom.new(:node, "b")),       # -> node(b).
    Literal.new(false, Atom.new(:edge, "a", "b"))   # -> edge(a,b).
]


# There also are some handy core extensions for converting Ruby data structures
#   to ASP objects

require "aspirator/extensions"

literals.merge({
    node: [["c"], ["d"], ["e"]],    # -> node(c). node(d). node(e).
    edge: [["b", "c"], ["b", "d"]]  # -> edge(b,c). edge(b,d).
}.to_asp)


# Run and use results

input_program = literals.to_program # -> node(a). node(b). edge(a). node(c).
                                    #       node(d). (...)

run = solver.run program + input_program

puts run.errors.inspect     # -> []
puts run.warnings.inspect   # -> []

as = run.answer_sets.first

puts as.include? Literal.new(false, Atom.new(:reachable_from, "d", "a"))
# -> true

puts as.include? Literal.new(false, Atom.new(:reachable_from, "e", "a"))
# -> false

puts run.answer_sets.asp_prettysets     # part of aspirator/extensions

```

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request



[1]: http://www.dlvsystem.com/
[2]: http://potassco.sourceforge.net/